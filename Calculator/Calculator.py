#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""


class SimpleComplexCalculator:
    """classe SimpleComplexCalculator"""

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def fsum(self):
        """somme des attributs complexes"""
        return [
            self.a[0] + self.b[0],
            self.a[1] + self.b[1],
        ]

    def substract(self):
        """difference des attributs complexes"""
        return [
            self.a[0] - self.b[0],
            self.a[1] - self.b[1],
        ]

    def multiply(self):
        """produit des attributs complexes"""
        return [
            self.a[0] * self.b[0]
            - self.a[1] * self.b[1],
            self.a[0] * self.b[1]
            + self.a[1] * self.b[0],
        ]

    def divide(self):
        """quotient des attributs complexes"""
        if (self.b[0] ** 2 + self.b[1] ** 2) != 0:
            return [
                (
                    self.a[0] * self.b[0]
                    + self.a[1] * self.b[1]
                )
                / (self.b[0] ** 2 + self.b[1] ** 2),
                (
                    self.a[1] * self.b[0]
                    + self.a[0] * self.b[1]
                )
                / (self.b[0] ** 2 + self.b[1] ** 2),
            ]
        return "Division par 0 !"

