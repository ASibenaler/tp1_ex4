## Exercice 4

# Objectif
L'objectif de cet exercice est de créer deux packages, l'un contenant la classe SimpleComplexCalculator, l'autre contenant le programme de test.

# Réalisation
On créé deux dossiers, l'un contenant la classe SimpleComplexCalculator, l'autre contenant le programme de test seul, dans lequel on importe la classe.

L'arborescence du projet est la suivante :

	.
	├── Calculator
	│   ├── Calculator.py
	│   ├── __init__.py
	│   └── __pycache__
	│       ├── Calculator.cpython-310.pyc
	│       └── __init__.cpython-310.pyc
	├── README.md
	└── Test
	    ├── ex4.py
	    └── __init__.py

On remarque qu'après avoir exécuté le programme de test, des fichiers *__init__.py* sont créés, ainsi qu'un repertoire *__pycache__*.

# Lancement
Le programme de test et la classe étant dans des paxkages différents, pour pouvoir exécuter le scipt ex4.py, il est nécessaire d'ajouter la racine du répertoire à la variable d'environnement PYTHONPATH :

	export PYTHONPATH=$PYTHONPATH:'.'
